package pm.gui;

import java.io.IOException;
import java.util.ArrayList;
import javafx.event.ActionEvent;
import javafx.scene.Cursor;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import static pm.PropertyType.BACK_ICON;
import static pm.PropertyType.BACK_TOOLTIP;
import static pm.PropertyType.ELLIPSE_ICON;
import static pm.PropertyType.ELLIPSE_TOOLTIP;
import static pm.PropertyType.FRONT_ICON;
import static pm.PropertyType.FRONT_TOOLTIP;
import static pm.PropertyType.RECTANGLE_ICON;
import static pm.PropertyType.RECTANGLE_TOOLTIP;
import static pm.PropertyType.REMOVE_ICON;
import static pm.PropertyType.REMOVE_TOOLTIP;
import static pm.PropertyType.SELECTION_TOOL_ICON;
import static pm.PropertyType.SELECTION_TOOL_TOOLTIP;
import static pm.PropertyType.SNAPSHOT_ICON;
import static pm.PropertyType.SNAPSHOT_TOOLTIP;
import pm.controller.PoseMakerController;
import static pm.controller.PoseMakerController.WORK_SHAPE_PATH;
import pm.data.DataManager;
import pm.file.FileManager;
import properties_manager.PropertiesManager;
import saf.ui.AppGUI;
import saf.AppTemplate;
import saf.components.AppWorkspaceComponent;
import saf.ui.AppMessageDialogSingleton;
import saf.ui.AppYesNoCancelDialogSingleton;

/**
 * This class serves as the workspace component for this application, providing
 * the user interface controls for editing work.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class Workspace extends AppWorkspaceComponent {

    // THESE CONSTANTS ARE FOR TYING THE PRESENTATION STYLE OF
    // THIS Workspace'S COMPONENTS TO A STYLE SHEET THAT IT USES
    static final String CLASS_MAX_PANE = "max_pane";
    static final String CLASS_BORDERED_PANE = "bordered_pane";
    static final String CLASS_TAG_BUTTON = "tag_button";
    static final String CLASS_TOOL_PANE = "edit_toolbar";
    static final String CLASS_HEADING_LABEL = "heading_label";
    static final String CLASS_RENDER_CANVAS = "render_canvas";

    // HERE'S THE APP
    AppTemplate app;

    // IT KNOWS THE GUI IT IS PLACED INSIDE
    AppGUI gui;

    // THIS HANDLES INTERACTIONS WITH PAGE EDITING CONTROLS
    //PageEditController pageEditController;
    // THESE ARE THE BUTTONS FOR ADDING AND REMOVING COMPONENTS
    VBox leftPane;
    Pane rightPane;
    HBox tagToolbar1;
    HBox tagToolbar2;
    VBox tagToolbar3;
    VBox tagToolbar4;
    VBox tagToolbar5;
    VBox tagToolbar6;
    HBox tagToolbar7;

    //Shape
    ArrayList<Shape> shapeGroup = new ArrayList<Shape>();
    ArrayList<Rectangle> rectangleGroup;
    ArrayList<Ellipse> ellipseGroup;
    Shape cursor;
    BorderPane myWorkspace;
    Rectangle cursorRectangle;
    Ellipse cursorEllipse;
    Color backgroundColor;
    Color fillColor;
    Color outlineColor;
    int thickness;
    double starting_point_x;
    double starting_point_y;
    double transX;
    double transY;
    boolean new_rectangle_is_being_drawn = false;
    boolean selected = false;
    Paint rememberColor;
    double rememberWidth;
    Shape lastSelectedShape;
    boolean new_ellipse_is_being_drawn = false;
    boolean new_shape;
    boolean new_rect;
    boolean new_ellipse;
    Group group = new Group();
    ColorPicker BG;
    Button ButtonMove;
    Button ButtonRemove;
    Button ButtonBack;
    Button ButtonFront;

    // HERE ARE OUR DIALOGS
    AppMessageDialogSingleton messageDialog;
    AppYesNoCancelDialogSingleton yesNoCancelDialog;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     *
     * @throws IOException Thrown should there be an error loading application
     * data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {

        // KEEP THIS FOR LATER
        app = initApp;

        // KEEP THE GUI FOR LATER
        gui = app.getGUI();

        Scene scene = gui.getPrimaryScene();
        // Get workspace from framework
        //BorderPane workspace = gui.getAppPane();  
        BorderPane myWorkspace = new BorderPane();

        //myWorkspace = new BorderPane();
        // FIRST THE LEFT HALF OF THE SPLIT PANE
        leftPane = new VBox();
        tagToolbar1 = new HBox();
        tagToolbar2 = new HBox();
        tagToolbar3 = new VBox();
        tagToolbar4 = new VBox();
        tagToolbar5 = new VBox();
        tagToolbar6 = new VBox();
        tagToolbar7 = new HBox();

        ButtonMove = gui.initChildButton(tagToolbar1, SELECTION_TOOL_ICON.toString(), SELECTION_TOOL_TOOLTIP.toString(), false);
        ButtonRemove = gui.initChildButton(tagToolbar1, REMOVE_ICON.toString(), REMOVE_TOOLTIP.toString(), false);
        Button ButtonRectangle = gui.initChildButton(tagToolbar1, RECTANGLE_ICON.toString(), RECTANGLE_TOOLTIP.toString(), false);
        Button ButtonCircle = gui.initChildButton(tagToolbar1, ELLIPSE_ICON.toString(), ELLIPSE_TOOLTIP.toString(), false);

        ButtonBack = gui.initChildButton(tagToolbar2, BACK_ICON.toString(), BACK_TOOLTIP.toString(), false);
        ButtonFront = gui.initChildButton(tagToolbar2, FRONT_ICON.toString(), FRONT_TOOLTIP.toString(), false);

        Label BackgroundColorLabel = new Label("Background Color");
        tagToolbar3.getChildren().add(BackgroundColorLabel);
        BG = new ColorPicker();
        tagToolbar3.getChildren().add(BG);
        //#ffef84*************************************************************************************************************
        //String orignalColor = rightPane.getStyle();
        Color c = Color.web("#ffef84");
        //rightPane.setStyle("-fx-background-color: #" + "ffef84" + ";");
        BG.setValue(c);
        
        //System.out.println(rightPane.getBackground().getFills().toString());
        Label FillColorLabel = new Label("Fill Color");
        //Button FC = new Button("FillColor");
        tagToolbar4.getChildren().add(FillColorLabel);
        ColorPicker FL = new ColorPicker();
        tagToolbar4.getChildren().add(FL);

        Label OutlineColorLabel = new Label("Outline Color");
        //Button OC = new Button("OutlineColor");
        tagToolbar5.getChildren().add(OutlineColorLabel);
        ColorPicker OL = new ColorPicker();
        tagToolbar5.getChildren().add(OL);
        OL.setValue(Color.BLACK);

        Label thicknessLabel = new Label("Outline Thickness");
        Slider thickness = new Slider(1, 10, 2);
        tagToolbar6.getChildren().add(thicknessLabel);
        tagToolbar6.getChildren().add(thickness);

        thicknessLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        OutlineColorLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        FillColorLabel.getStyleClass().add(CLASS_HEADING_LABEL);
        BackgroundColorLabel.getStyleClass().add(CLASS_HEADING_LABEL);

        Button photo = gui.initChildButton(tagToolbar7, SNAPSHOT_ICON.toString(), SNAPSHOT_TOOLTIP.toString(), false);

        leftPane.getChildren().add(tagToolbar1);
        leftPane.getChildren().add(tagToolbar2);
        leftPane.getChildren().add(tagToolbar3);
        leftPane.getChildren().add(tagToolbar4);
        leftPane.getChildren().add(tagToolbar5);
        leftPane.getChildren().add(tagToolbar6);
        leftPane.getChildren().add(tagToolbar7);

        rightPane = new Pane();
        rightPane.getChildren().add(group);

        // NOW FOR THE RIGHT For Canvas
        PoseMakerController PoseMakerController = new PoseMakerController((PoseMaker) app);
        //********************************CHANGE BACKGROUND COLOR************************************//
        BG.setOnAction((ActionEvent e) -> {
            String colorString = BG.getValue().toString();
            String substring = colorString.substring(2, colorString.length() - 2);
            System.out.println(colorString);
            rightPane.setStyle("-fx-background-color: #" + substring + ";");
            app.getGUI().updateToolbarControls(false);

        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        FL.setOnAction((ActionEvent e) -> {
            //cursor = cursorRectangle;
            if (selected == true) {
                cursor.setFill(FL.getValue());
                app.getGUI().updateToolbarControls(false);
            }

        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        OL.setOnAction((ActionEvent e) -> {
            //cursor = cursorRectangle;
            if (selected == true) {
                rememberColor = OL.getValue();
                app.getGUI().updateToolbarControls(false);
            }
            //cursor.setStroke(OL.getValue());

        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        thickness.valueProperty().addListener(
                (ov, curVal, newVal) -> {
                    if (selected == true) {
                        rememberWidth = thickness.getValue();
                        cursor.setStrokeWidth(thickness.getValue());
                    }
                    //
                    app.getGUI().updateToolbarControls(false);

                }
                
        );
        //********************************CHANGE BACKGROUND COLOR************************************//
        // TAKING A SNAPSHOT
        photo.setOnAction(e -> {

            PoseMakerController.saveAsPng();
        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        // Draw Rectangle
        ButtonRectangle.setOnAction(e -> {
            
            scene.getCursor();
            scene.setCursor(Cursor.CROSSHAIR);
            if (selected == true) {
                lastSelectedShape.setStroke(rememberColor);
                lastSelectedShape.setStrokeWidth(rememberWidth);
                selected = false;
                updataButton();
            }
            new_shape = true;
            new_rect = true;
            rightPane.setOnMousePressed((MouseEvent event) -> {
                if (new_rect == true) {
                    if (new_rectangle_is_being_drawn == false) {
                        starting_point_x = event.getX();
                        starting_point_y = event.getY();
                        new_rectangle_is_being_drawn = true;
                        cursorRectangle = new Rectangle();
                        shapeGroup.add(cursorRectangle);
                        PoseMakerController.saveShape(cursorRectangle);
                        DataManager dataManager = (DataManager) app.getDataComponent();
                        dataManager.setShapeGroupData(shapeGroup);
                        cursorRectangle.setX(starting_point_x);
                        cursorRectangle.setY(starting_point_y);
                        cursorRectangle.setFill(FL.getValue());
                        cursorRectangle.setStroke(OL.getValue());
                        cursorRectangle.setStrokeWidth(thickness.getValue());
                        group.getChildren().add(cursorRectangle);
                    }
                }
            });
            rightPane.setOnMouseDragged((MouseEvent event) -> {
                if (new_rect == true) {
                    if (new_rectangle_is_being_drawn == true) {
                        double current_ending_point_x = event.getX();
                        double current_ending_point_y = event.getY();
                        PoseMakerController.rectangleDrawing(starting_point_x, starting_point_y, current_ending_point_x, current_ending_point_y, cursorRectangle);
                    }
                }
            });
            rightPane.setOnMouseReleased((MouseEvent event) -> {
                if (new_rect == true) {
                    if (new_rectangle_is_being_drawn == true) {
                        new_rectangle_is_being_drawn = false;
                        //PoseMakerController.saveShape(cursorRectangle);
                    }
                }
            });
            app.getGUI().updateToolbarControls(false);
        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        // Draw Ellipse
        ButtonCircle.setOnAction(e -> {
            
            scene.getCursor();
            scene.setCursor(Cursor.CROSSHAIR);
            if (selected == true) {
                lastSelectedShape.setStroke(rememberColor);
                lastSelectedShape.setStrokeWidth(rememberWidth);
                selected = false;
                updataButton();
            }
            new_shape = true;
            new_ellipse = true;
            if (new_shape == true) {
                rightPane.setOnMousePressed((MouseEvent event) -> {
                    if (new_ellipse == true) {
                        if (new_ellipse_is_being_drawn == false) {
                            starting_point_x = event.getX();
                            starting_point_y = event.getY();
                            new_ellipse_is_being_drawn = true;
                            cursorEllipse = new Ellipse();
                            shapeGroup.add(cursorEllipse);
                            PoseMakerController.saveShape(cursorEllipse);
                            DataManager dataManager = (DataManager) app.getDataComponent();
                            dataManager.setShapeGroupData(shapeGroup);
                            cursorEllipse.setFill(FL.getValue());
                            cursorEllipse.setStroke(OL.getValue());
                            cursorEllipse.setStrokeWidth(thickness.getValue());
                            group.getChildren().add(cursorEllipse);

                        }
                    }
                });
                rightPane.setOnMouseDragged((MouseEvent event) -> {
                    if (new_ellipse == true) {
                        if (new_ellipse_is_being_drawn == true) {
                            double current_ending_point_x = event.getX();
                            double current_ending_point_y = event.getY();
                            PoseMakerController.ellipseDrawing(starting_point_x, starting_point_y, current_ending_point_x, current_ending_point_y, cursorEllipse);
                        }
                    }
                });
                rightPane.setOnMouseReleased((MouseEvent event) -> {
                    if (new_ellipse == true) {
                        if (new_ellipse_is_being_drawn == true) {
                            new_ellipse_is_being_drawn = false;
                            PoseMakerController.saveShape(cursorEllipse);
                        }
                    }
                });
            }
            app.getGUI().updateToolbarControls(false);
        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        ButtonMove.setOnAction((ActionEvent e) -> {
            
            scene.getCursor();
            scene.setCursor(Cursor.DEFAULT);
            new_shape = false;
            new_rect = false;
            new_ellipse = false;

            for (Shape working : shapeGroup) {
                working.setOnMouseClicked((MouseEvent event2) -> {
                    if (selected == true) {
                        lastSelectedShape.setStroke(rememberColor);
                        lastSelectedShape.setStrokeWidth(rememberWidth);
                    }
                    Object current2 = event2.getSource();
                    if (current2 instanceof Shape) {
                        cursor = (Shape) current2;
                        lastSelectedShape = (Shape) current2;
                        rememberColor = cursor.getStroke();
                        rememberWidth = cursor.getStrokeWidth();
                        thickness.setValue(cursor.getStrokeWidth());
                        OL.setValue((Color) cursor.getStroke());
                        FL.setValue((Color) cursor.getFill());
                        cursor.setStroke(Color.YELLOW);
                        //cursor.setStrokeWidth(4);
                        selected = true;
                        updataButton();
                        if (selected == true) {
                            if (cursor instanceof Rectangle) {
                                Shape tempCursor = cursor;
                                cursorRectangle = (Rectangle) tempCursor;

                                cursorRectangle.setOnMousePressed((MouseEvent event) -> {
                                    if (new_shape == false) {
                                        //cursorRectangle = (Rectangle)cursor;
                                        starting_point_x = cursorRectangle.getX();
                                        starting_point_y = cursorRectangle.getY();
                                    }
                                });
                                cursorRectangle.setOnMouseDragged((MouseEvent event) -> {
                                    if (new_shape == false) {
                                        transX = event.getX();
                                        transY = event.getY();
                                        cursorRectangle.setX(transX);
                                        cursorRectangle.setY(transY);
                                    }
                                });
                            } else if (cursor instanceof Ellipse) {
                                Shape tempCursor = cursor;
                                cursorEllipse = (Ellipse) tempCursor;
                                cursorEllipse.setOnMousePressed((MouseEvent event) -> {
                                    if (new_shape == false) {
                                        //cursorEllipse = (Ellipse)cursor;
                                        starting_point_x = cursorEllipse.getCenterX();
                                        starting_point_y = cursorEllipse.getCenterY();
                                    }
                                });
                                cursorEllipse.setOnMouseDragged((MouseEvent event) -> {
                                    if (new_shape == false) {
                                        transX = event.getX();
                                        transY = event.getY();
                                        cursorEllipse.setCenterX(transX);
                                        cursorEllipse.setCenterY(transY);
                                    }
                                });
                            }

                        }

                    }

                });
            }
            app.getGUI().updateToolbarControls(false);
        });

        //********************************CHANGE BACKGROUND COLOR************************************//
        ButtonFront.setOnAction((ActionEvent e) -> {
            Shape addFront = cursor;
            group.getChildren().remove(cursor);
            group.getChildren().add(addFront);
            
            shapeGroup.remove(cursor);
            shapeGroup.add(addFront);
            app.getGUI().updateToolbarControls(false);

        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        ButtonBack.setOnAction((ActionEvent e) -> {
            Shape addBack = cursor;
            group.getChildren().remove(cursor);
            group.getChildren().add(0, addBack);
            
            shapeGroup.remove(cursor);
            shapeGroup.add(0,addBack);
            app.getGUI().updateToolbarControls(false);

        });
        //********************************CHANGE BACKGROUND COLOR************************************//
        ButtonRemove.setOnAction((ActionEvent e) -> {
            group.getChildren().remove(cursor);
            app.getGUI().updateToolbarControls(false);

        });
        
        //********************************CHANGE BACKGROUND COLOR************************************//
        updataButton();
        myWorkspace.setLeft(leftPane);
        myWorkspace.setCenter(rightPane);
        workspace = new BorderPane();
        BorderPane workspace_2 = (BorderPane) workspace;
        workspace_2.setCenter(myWorkspace);

        //myWorkspace.prefHeightProperty().bind(workspace.heightProperty());
        //myWorkspace.prefWidthProperty().bind(workspace.widthProperty());        
        //this.workspace.setStyle("-fx-background-color: black;");
        //workspace.getChildren().add(myWorkspace);
        // NOTE THAT WE HAVE NOT PUT THE WORKSPACE INTO THE WINDOW,
        // THAT WILL BE DONE WHEN THE USER EITHER CREATES A NEW
        // COURSE OR LOADS AN EXISTING ONE FOR EDITING
        workspaceActivated = false;

    }

    public Pane getRightPane() {
        return rightPane;
    }

    public Group getGroup() {
        return group;
    }
    
    public void loadGroup(Group newGroup){
        group = newGroup;
        
        rightPane.getChildren().add(group);        
    }

    public ArrayList<Ellipse> getEllipse() {
        return ellipseGroup;
    }

    public ArrayList<Rectangle> getRectangle() {
        return rectangleGroup;
    }

    public ArrayList<Shape> getShape() {
        return shapeGroup;
    }
    
    public void setArrayList (ArrayList<Shape> array) {
        shapeGroup = array;
    }

    /**
     * This function specifies the CSS style classes for all the UI components
     * known at the time the workspace is initially constructed. Note that the
     * tag editor controls are added and removed dynamicaly as the application
     * runs so they will have their style setup separately.
     */
    @Override
    public void initStyle() {
        // NOTE THAT EACH CLASS SHOULD CORRESPOND TO
        // A STYLE CLASS SPECIFIED IN THIS APPLICATION'S
        // CSS FILE

        leftPane.getStyleClass().add(CLASS_MAX_PANE);
        tagToolbar1.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar2.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar3.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar4.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar5.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar6.getStyleClass().add(CLASS_BORDERED_PANE);
        tagToolbar7.getStyleClass().add(CLASS_BORDERED_PANE);
        rightPane.setStyle("-fx-background-color: #" + "ffef84" + ";");
        //workspace.getStyleClass().add(CLASS_MAX_PANE);
    }

    /**
     * This function reloads all the controls for editing tag attributes into
     * the workspace.
     */
    @Override
    public void reloadWorkspace() {
        group.getChildren().clear();
        rightPane.setStyle("-fx-background-color: #" + "ffef84" + ";");
        //Color c = Color.web("#f2f2f2");
        //BG.setValue(c);
//        BackgroundFill bgf = new BackgroundFill(Color.web("#ffef84"));
//        Background bg = new Background(bgf);
//        rightPane.setBackground(Background.EMPTY);

    }

    public String getBackgroundColor() {
        String colorString = BG.getValue().toString();
        String substring = colorString.substring(2, colorString.length() - 2);
        return substring;
    }

    public void setBackgroundColor(String color) {
        rightPane.setStyle("-fx-background-color: #" + color + ";");
        BG.setValue(Color.web(color));
    }
    
    public void updataButton(){
         //ButtonMove.setDisable(!selected);
         ButtonRemove.setDisable(!selected);
         ButtonBack.setDisable(!selected);
         ButtonFront.setDisable(!selected);        
          
    }
}
