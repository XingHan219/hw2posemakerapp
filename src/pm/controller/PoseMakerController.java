/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pm.controller;

import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.SnapshotParameters;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import pm.gui.Workspace;
import javafx.scene.shape.*;
import javax.imageio.ImageIO;
import pm.PoseMaker;
import pm.data.DataManager;
import pm.file.FileManager;
import properties_manager.PropertiesManager;
import saf.ui.AppMessageDialogSingleton;

/**
 *
 * @author xingye
 */
public class PoseMakerController {
    public static final String JSON_FILE = "shape.json";
    public static final String SHAPE_JSON_DATE_FILE_PATH = "./work/";
    public static final String WORK_SHAPE_PATH = SHAPE_JSON_DATE_FILE_PATH + JSON_FILE;
    public static final String PATH_IMAGES = "./images/";
    public static String SNAPSHOT_DONE = "SnapShot Successed";
    public static String SNAPSHOT = "SnapShot";
    // HERE'S THE FULL APP, WHICH GIVES US ACCESS TO OTHER STUFF
    PoseMaker app;
    ArrayList<Shape> shapeGroup = new ArrayList<Shape>();
    Group group;
    // WE USE THIS TO MAKE SURE OUR PROGRAMMED UPDATES OF UI
    // VALUES DON'T THEMSELVES TRIGGER EVENTS
    private boolean enabled;

        /**
     * Constructor for initializing this object, it will keep the app for later.
     *
     * @param initApp The JavaFX application this controller is associated with.
     */
    public PoseMakerController(PoseMaker initApp) {
	// KEEP IT FOR LATER
	app = initApp;
        //shapeGroup = new ArrayList<Shape>();
        //Workspace workspace = (Workspace) app.getWorkspaceComponent();
        //group = workspace.getGroup();
        //shapeGroup = workspace.getShape();
        
    }
    
        /**
     * This mutator method lets us enable or disable this controller.
     *
     * @param enableSetting If false, this controller will not respond to
     * workspace editing. If true, it will.
     */
    public void enable(boolean enableSetting) {
	enabled = enableSetting;
    }
    
    public String getBGcolor(){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        return workspace.getBackgroundColor();
    }
    
     public void setBGcolor(String color){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.setBackgroundColor(color);
    }
    
    public ArrayList<Shape> getShape() {
        return shapeGroup;
    }
    
    public void setShapeArray(ArrayList<Shape> shapeArray) {
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.setArrayList(shapeArray);
        
    }
    
    
    public void loadGroup(Group newGroup){
        Workspace workspace = (Workspace) app.getWorkspaceComponent();
        workspace.loadGroup(newGroup);
    }

    public void rectangleDrawing( double starting_point_x, double starting_point_y, double ending_point_x, double ending_point_y, Rectangle  given_rectangle){
          //Workspace workspace = (Workspace) app.getWorkspaceComponent();
          //Group group = workspace.getGroup();
          given_rectangle.setX( starting_point_x ) ;
          given_rectangle.setY( starting_point_y ) ;
          given_rectangle.setWidth( ending_point_x - starting_point_x ) ;
          given_rectangle.setHeight( ending_point_y - starting_point_y ) ;

          if ( given_rectangle.getWidth() < 0 ){
             given_rectangle.setWidth( - given_rectangle.getWidth() ) ;
             given_rectangle.setX( given_rectangle.getX() - given_rectangle.getWidth() ) ;
          }

          if ( given_rectangle.getHeight() < 0 ){
             given_rectangle.setHeight( - given_rectangle.getHeight() ) ;
             given_rectangle.setY( given_rectangle.getY() - given_rectangle.getHeight() ) ;
          }
        }
    
        public void saveShape(Shape shapeToSave){
            shapeGroup.add(shapeToSave);
        }
    
         public void ellipseDrawing(double starting_point_x, double starting_point_y, double ending_point_x, double ending_point_y, Ellipse  given_ellipse){
            //Workspace workspace = (Workspace) app.getWorkspaceComponent();
            //Group group = workspace.getGroup();
            
            double distanceX = (ending_point_x - starting_point_x);
            double distanceY = (ending_point_y - starting_point_y);
            double distanceX_abs = Math.abs(ending_point_x - starting_point_x);
            double distanceY_abs = Math.abs(ending_point_y - starting_point_y);
            given_ellipse.setCenterX(starting_point_x+0.5*distanceX);
            given_ellipse.setCenterY(starting_point_y+0.5*distanceY);
            given_ellipse.setRadiusX(0.5*distanceX_abs);
            given_ellipse.setRadiusY(0.5*distanceY_abs);
            
            if ( given_ellipse.getCenterX() < 0 ){
                given_ellipse.setCenterX(starting_point_x-0.5*distanceX);
            }
            if ( given_ellipse.getCenterY() < 0 ){
                given_ellipse.setCenterY(starting_point_y-0.5*distanceY);
            }
        }
         
         
        public void saveAsPng() {
            //Scene scene = app.getGUI().getPrimaryScene();
            Workspace workspace = (Workspace) app.getWorkspaceComponent();
            WritableImage image = workspace.getRightPane().snapshot(new SnapshotParameters(), null);

            // TODO: probably use a file chooser here
            File file = new File(PATH_IMAGES+"DrawingSnapShot.png");

            try {
                ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
                PropertiesManager props = PropertiesManager.getPropertiesManager();
		AppMessageDialogSingleton dialog = AppMessageDialogSingleton.getSingleton();
		dialog.show(props.getProperty(SNAPSHOT), props.getProperty(SNAPSHOT_DONE));
            } catch (IOException e) {
                // TODO: handle exception here
            }
        }
     }


