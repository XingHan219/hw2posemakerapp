package pm.file;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import javax.json.JsonWriterFactory;
import javax.json.stream.JsonGenerator;
import pm.data.DataManager;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.components.AppFileComponent;

/**
 * This class serves as the file management component for this application,
 * providing all I/O services.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class FileManager implements AppFileComponent {

    public static final String JSON_FILE = "shape.json";
    public static final String SHAPE_JSON_DATE_FILE_PATH = "./work/";
    public static final String WORK_SHAPE_PATH = SHAPE_JSON_DATE_FILE_PATH + JSON_FILE;

    static final String JSON_PANE_BACKGROUND_COLOR = "backgroundColor";
    static final String JSON_SHAPE = "shape";
    static final String JSON_RECT = "rect";

    static final String JSON_RECT_X = "rectX";
    static final String JSON_RECT_Y = "rectY";
    static final String JSON_RECT_H = "rectH";
    static final String JSON_RECT_W = "rectW";
    static final String JSON_RECT_COLOR = "rectColor";
    static final String JSON_RECT_OUTLINE_COLOR = "rectOoulineColor";
    static final String JSON_RECT_OUTLINE_WIDTH = "rectOutlineWidth";

    static final String JSON_ELLIPSE = "ellipse";

    static final String JSON_ELLIPSE_X = "ellipseX";
    static final String JSON_ELLIPSE_Y = "ellipseY";
    static final String JSON_ELLIPSE_RX = "ellipseRX";
    static final String JSON_ELLIPSE_RY = "ellipseRY";
    static final String JSON_ELLIPSE_COLOR = "ellipseColor";
    static final String JSON_ELLIPSE_OUTLINE_COLOR = "ellipseOoulineColor";
    static final String JSON_ELLIPSE_OUTLINE_WIDTH = "ellipseOutlineWidth";

    /**
     * This method is for saving user work, which in the case of this
     * application means the data that constitutes the page DOM.
     *
     * @param data The data management component for this application.
     *
     * @param filePath Path (including file name/extension) to where to save the
     * data to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void saveData(AppDataComponent data, String filePath) throws IOException {
        StringWriter sw = new StringWriter();
        DataManager dataManager = (DataManager) data;
        ArrayList<Shape> shapeGroup = dataManager.getShapeGroupData();
        String bgc = dataManager.getBackgroundColor();

        //JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        //JsonArray shapDataArray = arrayBuilder.build();
        JsonObject dataManagerJSO = Json.createObjectBuilder()
                .add(JSON_PANE_BACKGROUND_COLOR, makeBackgroundObkecet(bgc))
                .add(JSON_SHAPE, makeShapeJsonArray(shapeGroup))
                .build();

        // AND NOW OUTPUT IT TO A JSON FILE WITH PRETTY PRINTING
        Map<String, Object> properties = new HashMap<>(1);
        properties.put(JsonGenerator.PRETTY_PRINTING, true);
        JsonWriterFactory writerFactory = Json.createWriterFactory(properties);
        JsonWriter jsonWriter = writerFactory.createWriter(sw);
        jsonWriter.writeObject(dataManagerJSO);
        jsonWriter.close();

        // INIT THE WRITER
        OutputStream os = new FileOutputStream(filePath);
        JsonWriter jsonFileWriter = Json.createWriter(os);
        jsonFileWriter.writeObject(dataManagerJSO);
        String prettyPrinted = sw.toString();
        PrintWriter pw = new PrintWriter(filePath);
        pw.write(prettyPrinted);
        pw.close();

    }

    /**
     * This method loads data from a JSON formatted file into the data
     * management component and then forces the updating of the workspace such
     * that the user may edit the data.
     *
     * @param data Data management component where we'll load the file into.
     *
     * @param filePath Path (including file name/extension) to where to load the
     * data from.
     *
     * @throws IOException Thrown should there be an error reading in data from
     * the file.
     */
    @Override
    public void loadData(AppDataComponent data, String filePath) throws IOException {
        // CLEAR THE OLD DATA OUT
        DataManager dataManager = (DataManager) data;
        dataManager.reset();
        // LOAD THE JSON FILE WITH ALL THE DATA
        JsonObject json = loadJSONFile(filePath);
        //System.out.println("Test"); 
        JsonObject bgc = json.getJsonObject(JSON_PANE_BACKGROUND_COLOR);
        //System.out.println(bgc);
        dataManager.setBackgroundColor(bgc.getString(JSON_PANE_BACKGROUND_COLOR));
        //System.out.println(bgc);
        JsonArray jsonShapeArray = json.getJsonArray(JSON_SHAPE);
        //System.out.println(jsonShapeArray);
        loadJsonShapeArray(jsonShapeArray, dataManager);
        

    }
    
    private void loadJsonShapeArray(JsonArray jsonShapeArray, DataManager dataManager) {

        ArrayList<Shape> shapeGroup = dataManager.getShapeGroupData();
        Group group = new Group();
        for (int i = 0; i < jsonShapeArray.size(); i++) {
            JsonObject shapeJso = jsonShapeArray.getJsonObject(i);
            //String str = shapeJso.toString();
            //System.out.println(str);
            //System.out.println(shapeJso.getString(JSON_RECT_X));
            //test
            //System.out.println(shapeJso.toString().substring(2, 5));
            //System.out.println("Test");
            //JsonObject bgc1 = shapeJso.getJsonObject(JSON_RECT);
            //JsonArray jsonShapeArray1 = shapeJso.getJsonArray(JSON_RECT);
            //System.out.println(jsonShapeArray1);
            //System.out.println(shapeJso);
            //System.out.println(bgc1);
            
            if (shapeJso.toString().substring(2, 6).equals("rect")) {
                JsonObject rect = shapeJso.getJsonObject(JSON_RECT);
                Rectangle r  = new Rectangle();                
                System.out.println("done");
                r.setX(Double.parseDouble(rect.getString(JSON_RECT_X)));
                System.out.println(rect.getString(JSON_RECT_X));
                r.setY(Double.parseDouble(rect.getString(JSON_RECT_Y)));
                r.setHeight(Double.parseDouble(rect.getString(JSON_RECT_H)));
                r.setWidth(Double.parseDouble(rect.getString(JSON_RECT_W)));
                r.setFill(Color.web("#"+rect.getString(JSON_RECT_COLOR)));
                r.setStroke(Color.web("#"+rect.getString(JSON_RECT_OUTLINE_COLOR)));
                r.setStrokeWidth(Double.parseDouble(rect.getString(JSON_RECT_OUTLINE_WIDTH)));
                shapeGroup.add(r);
                group.getChildren().add(r);

            }
            else if(shapeJso.toString().substring(2, 6).equals("elli")){
                JsonObject ellipse = shapeJso.getJsonObject(JSON_ELLIPSE);
                Ellipse r  = new Ellipse();
                r.setCenterX(Double.parseDouble(ellipse.getString(JSON_ELLIPSE_X)));
                r.setCenterY(Double.parseDouble(ellipse.getString(JSON_ELLIPSE_Y)));
                r.setRadiusX(Double.parseDouble(ellipse.getString(JSON_ELLIPSE_RX)));
                r.setRadiusY(Double.parseDouble(ellipse.getString(JSON_ELLIPSE_RY)));
                r.setFill(Color.web("#"+ellipse.getString(JSON_ELLIPSE_COLOR)));
                r.setStroke(Color.web("#"+ellipse.getString(JSON_ELLIPSE_OUTLINE_COLOR)));
                r.setStrokeWidth(Double.parseDouble(ellipse.getString(JSON_ELLIPSE_OUTLINE_WIDTH)));
                shapeGroup.add(r);
                
                group.getChildren().add(r);
            }
        }
       // System.out.println("Test");
        dataManager.setShapeArray(shapeGroup);
        dataManager.loadGroupShape(group);

    }

    // HELPER METHOD FOR SAVING DATA TO A JSON FORMAT
    private JsonArray makeShapeJsonArray(ArrayList<Shape> shapeGroup) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();
        for (Shape shape : shapeGroup) {
            if (shape instanceof Rectangle) {
                Shape tempShap = shape;
                Rectangle rect = (Rectangle) tempShap;
                JsonObject jso = Json.createObjectBuilder()
                        .add(JSON_RECT, makeRectJsonObject(rect))
                        .build();
                arrayBuilder.add(jso);
            } else {
                Shape tempShap = shape;
                Ellipse ellipse = (Ellipse) tempShap;
                JsonObject jso = Json.createObjectBuilder()
                        .add(JSON_ELLIPSE, makeEllipseJsonObject(ellipse))
                        .build();
                arrayBuilder.add(jso);
            }
        }
        JsonArray jA = arrayBuilder.build();
        return jA;
    }

    private JsonObject makeBackgroundObkecet(String bgc) {
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_PANE_BACKGROUND_COLOR, bgc)
                .build();
        return jso;

    }

    private JsonObject makeRectJsonObject(Rectangle rect) {
        String colorStringFill = rect.getFill().toString();
        String fillColor = colorStringFill.substring(2, colorStringFill.length() - 2);
        String colorStringOutline = rect.getStroke().toString();
        String outlineColor = colorStringOutline.substring(2, colorStringOutline.length() - 2);

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_RECT_X, Double.toString(rect.getX()))
                .add(JSON_RECT_Y, Double.toString(rect.getY()))
                .add(JSON_RECT_H, Double.toString(rect.getHeight()))
                .add(JSON_RECT_W, Double.toString(rect.getWidth()))
                .add(JSON_RECT_COLOR, fillColor)
                .add(JSON_RECT_OUTLINE_COLOR, outlineColor)
                .add(JSON_RECT_OUTLINE_WIDTH, Double.toString(rect.getStrokeWidth()))
                .build();
        return jso;

    }

    private JsonObject makeEllipseJsonObject(Ellipse ellipse) {
        String colorStringFill = ellipse.getFill().toString();
        String fillColor = colorStringFill.substring(2, colorStringFill.length() - 2);
        String colorStringOutline = ellipse.getStroke().toString();
        String outlineColor = colorStringOutline.substring(2, colorStringOutline.length() - 2);

        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_ELLIPSE_X, Double.toString(ellipse.getCenterX()))
                .add(JSON_ELLIPSE_Y, Double.toString(ellipse.getCenterY()))
                .add(JSON_ELLIPSE_RX, Double.toString(ellipse.getRadiusX()))
                .add(JSON_ELLIPSE_RY, Double.toString(ellipse.getRadiusY()))
                .add(JSON_ELLIPSE_COLOR, fillColor)
                .add(JSON_ELLIPSE_OUTLINE_COLOR, outlineColor)
                .add(JSON_ELLIPSE_OUTLINE_WIDTH, Double.toString(ellipse.getStrokeWidth()))
                .build();
        return jso;

    }

    // HELPER METHOD FOR SAVING DATA TO A JSON FORMAT
    private JsonArray buildJsonArray(ArrayList<String> data) {
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for (String d : data) {
            jsb.add(d);
        }
        JsonArray jA = jsb.build();
        return jA;
    }

    // HELPER METHOD FOR LOADING DATA FROM A JSON FORMAT
    private JsonObject loadJSONFile(String jsonFilePath) throws IOException {
        InputStream is = new FileInputStream(jsonFilePath);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        is.close();
        return json;
    }

    /**
     * This method exports the contents of the data manager to a Web page
     * including the html page, needed directories, and the CSS file.
     *
     * @param data The data management component.
     *
     * @param filePath Path (including file name/extension) to where to export
     * the page to.
     *
     * @throws IOException Thrown should there be an error writing out data to
     * the file.
     */
    @Override
    public void exportData(AppDataComponent data, String filePath) throws IOException {
        File shape = new File(SHAPE_JSON_DATE_FILE_PATH);
        shape.mkdir();
        saveData(data, filePath);
    }

    /**
     * This method is provided to satisfy the compiler, but it is not used by
     * this application.
     */
    @Override
    public void importData(AppDataComponent data, String filePath) throws IOException {
        // NOTE THAT THE Web Page Maker APPLICATION MAKES
        // NO USE OF THIS METHOD SINCE IT NEVER IMPORTS
        // EXPORTED WEB PAGES
    }
}
