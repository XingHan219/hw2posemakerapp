package pm.data;

import java.util.ArrayList;
import javafx.scene.Group;
import javafx.scene.shape.Shape;
import pm.PoseMaker;
import pm.controller.PoseMakerController;
import pm.file.FileManager;
import pm.gui.Workspace;
import saf.components.AppDataComponent;
import saf.AppTemplate;

/**
 * This class serves as the data management component for this application.
 *
 * @author Richard McKenna
 * @author ?
 * @version 1.0
 */
public class DataManager implements AppDataComponent {

    // THIS IS A SHARED REFERENCE TO THE APPLICATION
    AppTemplate app;
    ArrayList<Shape> shapeGroup;
    Group group;
    Workspace workspace;
    PoseMakerController poseMakerController;

    /**
     * THis constructor creates the data manager and sets up the
     *
     *
     * @param initApp The application within which this data manager is serving.
     */
    public DataManager(AppTemplate initApp) throws Exception {
        // KEEP THE APP FOR LATER
        app = initApp;
        poseMakerController = new PoseMakerController((PoseMaker) app);
        shapeGroup = poseMakerController.getShape();
    }

    public ArrayList<Shape> getShapeGroupData() {
        return shapeGroup;
    }
    public void setShapeArray (ArrayList<Shape> shapeArray) {
        shapeGroup = shapeArray;
        poseMakerController.setShapeArray(shapeArray);
    }

    public void setShapeGroupData(ArrayList<Shape> newShapeGroup) {
        shapeGroup = newShapeGroup;
    }

    public String getBackgroundColor() {
        //poseMakerController = new PoseMakerController((PoseMaker) app);
        return poseMakerController.getBGcolor();
    }
    
    public void setBackgroundColor(String color) {
        //poseMakerController = new PoseMakerController((PoseMaker) app);
        poseMakerController.setBGcolor(color);
    }
    public void loadGroupShape(Group newGroup){
        //poseMakerController = new PoseMakerController((PoseMaker) app);
        poseMakerController.loadGroup(newGroup);
    }

    /**
     * This function clears out the HTML tree and reloads it with the minimal
     * tags, like html, head, and body such that the user can begin editing a
     * page.
     */
    @Override
    public void reset() {
        //shapeGroup.reset();
        //shapeGroup.clear();
        // group.getChildren().clear();
        shapeGroup.clear();

    }
}
